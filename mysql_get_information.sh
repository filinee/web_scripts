#!/bin/bash
# Showing main information from MySQL
#

source ./sharedfunc.sh
source ./web_site_specific_data.sh


function show_general_information(){

if_process_started "$db_service_name"
	mysql_status=$?

if [[ mysql_status -eq 1 ]]
then
		
mysql -u${mysql_root_username} -p"${mysql_pass}" -e "SELECT db, group_concat(user order by db) FROM mysql.db GROUP BY db;"
mysql -u${mysql_root_username} -p"${mysql_pass}" -e "SELECT user, group_concat(db order by user) FROM mysql.db GROUP BY user;"

mysql -u${mysql_root_username} -p"${mysql_pass}" -e "SELECT * FROM information_schema.schemata;" 

# SELECT schema_name FROM information_schema.schemata;

	
else
	echo "MySQL is not started" >&2
exit 1
fi

}


function show_all_info(){

sql_user_list=`mysql -u${mysql_root_username} -p"${mysql_pass}" -e "SELECT db, group_concat(user order by db) FROM mysql.db GROUP BY db;"`
sql_db_list=`mysql -u${mysql_root_username} -p"${mysql_pass}" -e "SELECT schema_name FROM information_schema.schemata;"`


	for str in ${sql_user_list[@]}
		do
			show_user_information $str
		done

	for str in ${sql_db_list[@]}
		do
			show_db_information $str
		done


}

function show_db_information(){
table_name=$1
mysql -u${mysql_root_username} -p"${mysql_pass}" -e "SELECT TABLE_SCHEMA, table_name, DATA_LENGTH, UPDATE_TIME, CREATE_TIME  FROM information_schema.tables WHERE TABLE_SCHEMA='${table_name}';"
} 

function show_user_information(){
user_name=$1
mysql -u${mysql_root_username} -p"${mysql_pass}" -e "SELECT Host, User, Password, Select_priv, Insert_Priv, Update_Priv,Delete_Priv,Drop_priv FROM mysql.user WHERE user='${user_name}';"
mysql -u${mysql_root_username} -p"${mysql_pass}" -e "SHOW GRANTS FOR '${user_name}'@'localhost';"
mysql -u${mysql_root_username} -p"${mysql_pass}" -e "SELECT User,Password from mysql.user WHERE User='${user_name}' GROUP BY User ;"
}


while getopts ad:u: option
do
	case "${option}"
	in
		d) databasename="$OPTARG" ;;
		u) username="$OPTARG" ;;
		a) allinfo="all";;
		\?) echo "-d databasename -u username" >&2
		    exit 1;;
	esac
done


if [[ -n $databasename || -n $username || -n $allinfo ]] 
then
if [[ -n $username ]]; then show_user_information $username; fi
if [[ -n $databasename ]]; then show_db_information $databasename; fi
if [[ -n $allinfo ]]; then show_all_info; fi
else
show_general_information
fi
