#!/bin/bash
#Description
#Add user to ftp
#echo 'Аргументы:-> ' $@ >&2
#echo "Количество аргументов->"$# >&2

source ./sharedfunc.sh
source ./web_site_specific_data.sh

# Функция создания нового пользователя
function create_user(){
	# Имя пользователя передается в параметрах
	local name=$1

	# Проверка на наличие пользователя в системе
	check_user_name $1
	user_error=$?
	
	if [ "$user_error" -eq "0" ]
	then
		terminal="/sbin/nologin"
		comment="web user $name"
	
	# Новый пароль по умолчанию если файл с генерацией паролей недоступен
	pass=`./my_passgen`
	if [ -z "$pass" ]
	then
		pass="123456"
	fi
	
	echo "Adding a new user: $name to the system"  >&2
	
	date_now=`date +%d.%m.%y\ %T`
	echo "$date_now linux user name $name password: $pass" >> web.log
	# Adding a new user without teminal and (/sbin/nologin) and
	# home folder (-M) also account wount block after password expired
	# (-f -1)
	`/usr/sbin/useradd  -s "$terminal" -M -f -1 -c "$comment" $name`
	
	echo "Seting up a new user's password: $pass" >&2
	`echo -e "$pass\n$pass\n" | /usr/bin/passwd $name &> /dev/null`
	
	else
	echo "This user already exist" >&2
	return 1
	fi
}


while getopts :u: option
do
	case "${option}"
	in
		u) username="$OPTARG" ;;
		\?) echo "Usage -u username" >&2
		    exit 1;;
		:) echo "Option $OPTARG require an argument"  >&2
	esac
done


# Запуск процедуры создания пользователя
create_user $username

