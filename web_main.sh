#!/bin/bash
# Creating of new web site, users, folders, mysql etc

# my_passgen - Password generation
# sharedfunc.sh - some shared function

usagestring="$0 -u username -f foldername -d databasename -m mailofadmin"

if [[ $# -lt 3 ]]
 then
   echo "There are no any arguments"
   echo "$usagestring"
   exit 1
fi

while getopts :u:f:d:m: option
do
	case "${option}"
	in
		u) username="$OPTARG" ;;
		f) foldername="$OPTARG" ;;
		d) database="$OPTARG" ;;
		m) mailofadmin="$OPTARG" ;;
		\?) echo "$usagestring">&2 
		    exit 1;;
		:) echo "Option $OPTARG require an argument" >&2
		    exit 1;;
	esac
done

sitename=`basename "$foldername"`
scriptdir=`dirname $0`
empty=$empty

# Creating a Linux user user 
if [[ $username != $empty ]]
then
./web_create_linux_user.sh -u "$username"
else
echo "Error1: New Linux user couldn't be created"
fi

#Create a web folder
if [[ $username != $empty && $foldername != $empty ]]
then
`./web_create_linux_folder.sh -f "$foldername" -u "$username"`
else
echo "Error2: New folder could not be generated"
fi

#Changing a folder owner
if [[ $foldername != $empty ]]
then
`./web_change_folder_owner.sh -f "$foldername"`
else
echo "Error3: We can not change folder owner"
fi

#Setting up a ftp user
if [[ $username != $empty && $foldername != $empty ]]
then
`./web_ftp_setup.sh -u "$username" -f "$foldername"`
else
echo "Error4: ftp user could not be generated"
fi

#Create mysqldatabase and user.
if [[ $username != $empty && $database != $empty ]]
then
`./web_create_sql_db_user.sh -d "$database" -u "$username"`
else
echo "Error5: Data base could not be generated"
fi

# Generating a new vir host file
if [[ $foldername != $empty && $sitename != $empty ]]
then
`./web_generate_virtual_host_template.sh -s "$sitename" -f "$foldername" -m "$mailofadmin"`
else
echo "Error6: Host file could not be genereted"
fi


# Restarting services
`./web_restart_services.sh`

exit 0
