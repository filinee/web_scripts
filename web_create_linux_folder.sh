#!/bin/bash

#Creating a web folder for specific user

source ./sharedfunc.sh
source ./web_site_specific_data.sh



# web службы необходимо вписать через запятую

# Функция создания папки
function create_folder(){
	
	local foldername=$1
	
	
	# Проверка на наличие папки в системе
	if [[ ! -d $foldername ]]
	then
	echo "Creating a new web folder: $foldername" >&2
		`mkdir $foldername`
	else 
	echo "Folder already exist" >&2
	return 1
	fi
}

# Функция измененеия прав на папку
function change_rights(){
	
	# Получение параметров переданных в фукнцию
	local foldername=$1
	local username=$2
	local webgrp=$3
	
	check_user_name $username
	local user_exist=$?
	
	if [[ -d $foldername && $user_exist -eq 1 ]]
	then
	echo "Changing owner of the folder: $foldername" >&2
		`chown $username $foldername`
	echo "Changing group of the folder: $foldername" >&2
		`chgrp $username $foldername`
# Добавление в группу пользователя web служб
		for web_users in ${webserviceusers[@]}
		do
			#`groupmems -g $username -a $web_users`
			`/usr/sbin/usermod -a -G $username $web_users`
		done
	else
		echo "Folder or user dosn't exist" >&2
	fi
}


while getopts :f:u: option
do
        case "${option}"
        in
		f) foldername="$OPTARG" ;;
		u) username="$OPTARG" ;;
                \?) echo "-f foldername \n  -u username" >&2
                    exit 1;;
		:) echo "Option $OPTARG require an argument"  >&2
        esac
done


if [[ -n $foldername && -n $username ]] 
then
	create_folder $foldername
	change_rights $foldername $username $webgrp
else
echo "Folder name or user name were not get" >&2
fi
