#!/bin/bash
# Shared function

source ./sharedfunc.sh
source ./web_site_specific_data.sh

function redirect_user_home_directory(){
if_process_started "$ftp_service_name"
ftp_proc_status=$?
if [[ $ftp_proc_status -eq 1 ]]
then
	local username=$1
	local new_home_folder=$2
		`/usr/sbin/usermod --home "${new_home_folder}" "${username}"`
	else
echo "Process of ftp wasn't started">&2
fi
}


while getopts :u:f: option
do
	case "${option}"
	in
		u) username="$OPTARG" ;;
		f) foldername="$OPTARG" ;;
		\?) echo "Usage -u username -f foldername" >&2
		    exit 1;;
		:) echo "Option $OPTARG require an argument" >&2
	esac
done

redirect_user_home_directory "$username" "$foldername"
