#!/bin/bash
#
# Script for adding a new mysql users and mysql database
#

source ./sharedfunc.sh
source ./web_site_specific_data.sh

function check_mysqluser_exist(){
	mysqluser=$1
	
	if_process_started "$db_service_name"
	mysql_status=$?
	
	if [[ mysql_status -eq 1 ]]
	then
	sql_user_list=`mysql -u${mysql_root_username} -p"${mysql_pass}" -e "SELECT User,Password FROM mysql.user;"|awk '{print $1}'`
		
	for str in ${sql_user_list[@]}
		do
				if [[ "$str" == "$mysqluser" ]]
				then
				uexist=1
				break
				else
				uexist=0
				fi
		done
	
	if [[ "$uexist" -eq 1 ]]
	then
		return 1
	else
		return 0
	fi
	
	else
	echo "MySQL is not started" >&2
	fi
}
# Создание пользователя MySQL
function createmysqluser() {
if_process_started "$db_service_name"

mysql_status=$?


if [[ mysql_status -eq 1 ]]
then

	local username=$1
	# Скрипт с функцией генерации паролей
	check_mysqluser_exist $username
	local user_status=$?

	# Пользователь создается автоматически при предоставлении полномочий
	# Если пользователь существует пароль в запросе не передается
	# Если пользователь новый то генерируется пароль
	if [[ $user_status -eq 1 ]]
	then
		echo "Changing user rights: $username for MySQL Server" >&2
		create_user_req="GRANT USAGE ON *.* TO '$username'@'localhost';"
	else	
		local pass=`./my_passgen`
		create_user_req="GRANT USAGE ON *.* TO '$username'@'localhost' IDENTIFIED BY '$pass';"
		#create_user_req="CREATE USER '$username'@'localhost' IDENTIFIED BY '$pass';" 	
		echo "Creating a new user $username/$pass for MySQL Server" >&2
		# Recording to log
		date_now=`date +%d.%m.%y\ %T`
		echo "$date_now mysql user name: $username password: $pass" >> web.log
	fi
		`mysql -u${mysql_root_username} -p"${mysql_pass}" -e "$create_user_req"`
	return 0
else
echo "MySQL is not started" >&2
exit 1
fi
}
# Создание базы данных сайта
function create_mysql_base(){
	if_process_started "$db_service_name"
	mysql_status=$?
	
	if [[ mysql_status -eq 1 ]]
	then
	
		local username=$1
		local databasename=$2
		# Формирование запроса к SQL
		create_db_req="CREATE DATABASE IF NOT EXISTS ${databasename} CHARACTER SET utf8 COLLATE utf8_general_ci;"
		grant_priv_req="GRANT ALL PRIVILEGES ON ${databasename}.* TO '$username'@'localhost' WITH GRANT OPTION;"
		#Clear of privelege cashe
		flush_priv_req="FLUSH PRIVILEGES;"
		
		echo "Creating a database $databasename and granting priveleges for user $username" >&2
		sql_req="${create_db_req}${grant_priv_req}${flush_priv_req}"
		
		`mysql -u${mysql_root_username} -p"${mysql_pass}" -e "${sql_req}"`
		
		date_now=`date +%d.%m.%y\ %T`
		echo "$date_now mysql database: $databasename for user: $username" >> web.log
		return 0
	else
	echo "MySQL is not started" >&2
	fi
}

# Обработка параметров переданных запросу
while getopts :d:u: option
do
	case "${option}"
	in
		d) databasename="$OPTARG" ;;
		u) username="$OPTARG" ;;
		\?) echo "-f databasename -u username" >&2
		    exit 1;;
		:) echo "Option $OPTARG require an argument"  >&2
	esac
done

# Запуск функций создания базы данных и пользователя в случае наличия имени базы и пользователя
if [[ -n $databasename && -n $username ]] 
then
createmysqluser $username
create_mysql_base $username $databasename 
else
echo "Database or user name were not get" >&2

fi
