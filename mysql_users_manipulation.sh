#!/bin/bash

# Manipulation with MysqlUsers
# Set a new password
# Delete user
# Status: finished


source ./sharedfunc.sh
source ./web_site_specific_data.sh

# Set a new password for mysql user
#==========================================================
function set_new_password(){
	mysqluser=$1
	pass=$2
	
# Check if mysql process started
	if_process_started "$db_service_name"
	mysql_status=$?

# Set a new password
	if [[ mysql_status -eq 1 ]]
	then
		mysql -u${mysql_root_username} -p"${mysql_pass}" -e "SET PASSWORD FOR '${mysqluser}'@'${site}'=PASSWORD('${pass}');"
	else
		echo "MySQL is not started" >&2
		exit 1
	fi
}
#=======================================================


# Delete mysql users
#======================================	
function delete_user(){
	mysqluser=$1

# Check if mysql process started
	if_process_started "$db_service_name"
	mysql_status=$?

# Do delete
	if [[ mysql_status -eq 1 ]]
	then
	mysql -u${mysql_root_username} -p"${mysql_pass}" -e "REVOKE ALL PRIVILEGES, GRANT OPTION FROM ${mysqluser}@${site};"	
	mysql -u${mysql_root_username} -p"${mysql_pass}" -e "DROP USER '${mysqluser}'@'${site}';"
	else
		echo "MySQL is not started" >&2
		exit 1
	fi
}
#====================================


# Checking options for script
while getopts :s:p:u:d: option
do
	case "${option}"
	in
		p) pass="$OPTARG" ;;
		s) site="$OPTARG" ;;
		u) username="$OPTARG" ;;
		d) delete_user_name="$OPTARG" ;;
		\?) echo "-p new pass (to change password)"
		    echo "-s user's site (optional, localhost by default)">&2
		    echo "-u username" >&2
		    echo "-d username (for delete user)" >&2
		    exit 1;;
		:) echo "Option $OPTARG require an argument"  >&2
	esac
done

# We could not set a new pass and delete user this same time
if [[ (-n $username || -n $pass) && -n $delete_user_name ]]
then
echo "Too many arguments" >&2 
 echo "-s user's site (optional, localhost by default)">&2
 echo "-p new pass (to change password)">&2
 echo "-u username" >&2
 echo "-d username (for delete user)" >&2
exit 1
fi

# Set up a default site
if [[ -z $site ]]; then site="localhost" ; fi

# IF we have user name and pass, just set a new pass
if [[ -n $username && -n $pass ]]
then
	set_new_password $username $pass
fi


# IF we have user name and delete option, we delete user
if [[ -n $delete_user_name ]]
then
	delete_user $delete_user_name
fi
