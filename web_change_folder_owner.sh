#!/bin/bash
#This script is changing subfolders and files owners and group
#base on root folder owner and group

function changing_owner_and_grp_subfolder(){
	local foldername=$1

if [[ -d $foldername ]]	
then
	#Getting of owner and froup of folder
	local folder_username=`ls -la $foldername | head -n2 | tail -n 1 | awk '{print $3}'`
	local folder_group=`ls -la $foldername | head -n2 | tail -n 1 | awk '{print $4}'`
	
	if [[ -n $folder_username && -n $folder_group ]]
	then

		echo "Finding of files and folders and change their rights" >&2
		`find $foldername -exec chown -R $folder_username:$folder_group '{}' \;`
		`find $foldername -exec chmod 770 '{}' \;`
	else
		echo "Scripts could not get user name or group for folder $foldernamei" >&2
		exit 1
	fi	
	
else
	echo "Folder $foldername dosn't exist" >&2
	exit 1
fi	
}

while getopts :f: option
do
        case "${option}"
        in
		f) foldername="$OPTARG" ;;
                \?) echo 'This script reqire a param like "-f foldername"'>&2
                    exit 1;;
		:) echo "Option $OPTARG require an argument">&2
		    exit 1;;
        esac
done

changing_owner_and_grp_subfolder "$foldername"
