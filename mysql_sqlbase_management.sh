#!/bin/bash
#
# Delte MySQL db
# Clear mySQL db
# Rename table
# Create dump

if [[ $# -lt 1 ]]
 then
   echo "There are no any arguments"
   echo "Please look at $0 -h "
   exit 1
fi


source ./sharedfunc.sh
source ./web_site_specific_data.sh

#Deleting db
function delete_mysql_db(){
	db_name=$1
	mysql -u${mysql_root_username} -p"${mysql_pass}" -e "DROP DATABASE IF EXISTS "${db_name}";" 
}
#==============================


#Clearing db
function clear_mysql_db(){
	db_name=$1
	delete_mysql_db $db_name
	mysql -u${mysql_root_username} -p"${mysql_pass}" -e "CREATE DATABASE IF NOT EXISTS "${db_name}" CHARACTER SET utf8 COLLATE utf8_general_ci;"
}
#==============================


#Clone db
function clone_mysql_db(){	
dbname=$1
to_db_name=$2
mysql -u${mysql_root_username} -p"${mysql_pass}" -e "CREATE DATABASE IF NOT EXISTS ${to_db_name} CHARACTER SET utf8 COLLATE utf8_general_ci;"
mysqldump --skip-add-locks --disable-keys --extended-insert --skip-lock-tables -u${mysql_root_username} -p"${mysql_pass}" "${dbname}"  | mysql  -u${mysql_root_username} -p"${mysql_pass}"  "${to_db_name}" 
}
#==============================


# Creating dump 11
function create_dump(){
	dbname=$1
	filename=$2
	host=$3
	oth_param=$4

		# Dump will return 1 and break follow execution
	if [[ -z $dbname  ]]; then return 1; fi

		# Default values
	if [[ -z $host  ]]; then host="localhost"; fi
	
	if [[ -z $filename  ]]
	then
		prefix=`date +%Y_%m_%d_%k_%M`
		filename="${prefix/ /}.${dbname}.sql"	 
	fi

		# Dump execution
	mysqldump  ${oth_param} -h"${host}" -u"${mysql_root_username}" -p"${mysql_pass}" --databases "${dbname}" > "${filename}"
	}

# To restore use follow command:
#`mysql -u${mysql_root_username} -p"${mysql_pass}" "${dbname}"  < ${filename}`



while getopts d:c:r:b:u:t: option
do
	case "${option}"
	in
		r) deletedb="$OPTARG" ;;
		w) cleardb="$OPTARG" ;;
		c) clonedb="$OPTARG" ;;
		d) dumpdb="$OPTARG" ;;
		t) target_db="$OPTARG";;
		\?) echo "-r remove db" >&2
		    echo "-w clean out db" >&2
		    echo "-c clonedb [from]->[to]" >&2
		    echo "-d dumpdb db " >&2
		    echo "-t target db name, use inly with -r and -n parametrs" >&2
		    exit 0;;
		*) echo "There should parametrs";;
	esac
done


if [[ -n $deletedb ]]; then delete_mysql_db $deletedb; fi
if [[ -n $cleardb ]]; then clear_mysql_db $cleardb; fi
if [[ -n $clonedb  && -n $target_db ]]; then clone_mysql_db $clonedb $target_db; fi
if [[ -n $dumpdb ]]
 then
	prefix=`date +%Y_%m_%d_%k_%M`
	filename="${prefix/ /}.${dumpdb}.sql"
	host="localhost"
	oth_param="--add-drop-database --add-drop-table --skip-add-locks --create-options --disable-keys --extended-insert --skip-lock-tables"
	create_dump $dumpdb $filename $host "${oth_param}"

 fi

