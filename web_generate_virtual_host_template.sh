#!/bin/bash
# Generation a web virtual host file based on foder name, dns address, admin mail adress
# This script use file virtual_host_template.conf
#


source web_site_specific_data.sh

function generate_apache_file() {
#Getting information
site_dns_name=$1
site_local_folder=$2
site_admin_email=${3:-"admin@$site_dns_name"}
# If necesary information weren't given we should break this script
if [[ -n $site_dns_name && -n $site_local_folder ]]
then
OLD_IFS="$IFS"
IFS=$'\n'
# Getting template file into array
site_template=`cat virtual_host_template.conf`

template_of_dns_name="=site_dns_name="
template_of_site_folder="=site_root_folder="
template_of_admin_mail="=site_admin_email="


for string in ${site_template[@]}
do

case "$string" in
	 *$template_of_dns_name*	) string=${string/"$template_of_dns_name"/"$site_dns_name"};; 
	 *$template_of_site_folder*	) string=${string/"$template_of_site_folder"/"$site_local_folder"};; 
	 *$template_of_admin_mail*	) string=${string/"$template_of_admin_mail"/"$site_admin_email"};; 
esac

# Generating file
echo $string >> 1_virtual_${site_dns_name/./}.conf
done

mv 1_virtual_${site_dns_name/./}.conf $path_to_apache_vh_folder
IFS="$OLD_IFS"
else

exit 1

fi
}
# Processing patametrs
while getopts :s:f:m: option
do
	case "${option}"
	in
		s) sitename="$OPTARG" ;;
		f) foldername="$OPTARG" ;;
		m) adminmail="$OPTARG" ;;
		\?) echo "This script can use -s for site, -f for folder and -m[optional] for admin mail" >&2 
		    exit 1;;
		:) echo "Option $OPTARG require an argument" >&2
	esac
done


generate_apache_file $sitename $foldername $adminmail
