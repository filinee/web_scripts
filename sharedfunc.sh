#!/bin/bash

#cat /etc/group |awk -F: '{print $1}'

function check_group_name (){
	# Получаем первый параметр переданной функции
	local groupname=$1
	
	# Читаем в массив файл /etc/passwd
	grp_file_arr=(`awk -F ":" '{ print $1 }' /etc/group`)
	
	# Разбираем файл и сравниваем его с введенным значением, если такое имя уже есть
	for str in ${grp_file_arr[@]}
		do
				if [[ "$str" == "$groupname" ]]
				then
				gexist=1
				break
				else
				gexist=0
				fi
		done
	
	if [[ gexist -eq 1 ]]
	then
		return 1
	else
		return 0
	fi
}

function check_user_name (){
	# Получаем первый параметр переданной функции
	username=$1
	
	# Читаем в массив файл /etc/passwd
	passwd_file_arr=(`awk -F ":" '{ print $1 }' /etc/passwd`)
	
	# Разбираем файл и сравниваем его с введенным значением, если такое имя уже есть
	for str in ${passwd_file_arr[@]}
		do
				if [[ "$str" == "$username" ]]
				then
				uexist=1
				break
				else
				uexist=0
				fi
		done
	
	if [[ uexist -eq 1 ]]
	then
		return 1
	else
		return 0
	fi
}

function if_process_started(){
	program_name=$1
	PID=`ps -aef | grep $program_name | grep -v grep | awk '{print $2}'| tail -n 1`
	
	if [[ ${PID} -gt 0 ]]
	then
		return 1 #program is stared
	else
		return 0 #program is not started
	fi
}
