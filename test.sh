#!/bin/bash

while getopts :d:c:r:n:u: option
do
	case "${option}"
	in
		d) deletedb="$OPTARG" ;;
		\?) echo "-d db name for deleting" >&2
		    echo "-c db name for clearing" >&2
		    echo "-r clonedb [from]->[to]" >&2
		    echo "-n rename db " >&2
		    echo "-u delete user name" >&2
		    exit 0;;
	esac
done

echo $deletedb
